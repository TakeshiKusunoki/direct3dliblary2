struct VS_OUT
{
	float4 position : SV_POSITION;
	float4 color : COLOR;
	float2 texcoord : TEXCOORD;
	// UNIT.20
	//float4 bone_weights : WEIGHTS;
	//uint4 bone_indices : BONES;
};

// UNIT.20
struct PS_OUT
{
	float4 position : SV_POSITION;
	float4 color : COLOR;
	float2 texcoord : TEXCOORD;
	float3 PosSM: POSITION_SM; // 頂点座標(シャドウマップの透視座標系)
	//float4 normal : NORMAL;
};

#define MAX_BONES 32
cbuffer CONSTANT_BUFFER : register(b0)
{
	row_major float4x4 world_view_projection;//wvp
	row_major float4x4 world;	//ワールド座標
	float4 material_color;	//物体の色
	float4 light_direction;	//ライト進行方向
	// UNIT.21
	row_major float4x4 bone_transforms[MAX_BONES];//ボーン行列
	float4 eyePos;            //視点座標
	matrix SMWorldViewProj; // ワールド×ビュー×透視変換行列(シャドウマップ用)
};