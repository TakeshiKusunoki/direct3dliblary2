#include "ShadowShader.hlsli"

Texture2D diffuse_map:register(t0);
SamplerState diffuse_map_sampler_state:register(s0);// テクスチャ・サンプラ
Texture2D ShadowMap : register(t1); // シャドウマップ
SamplerState smpBorder : register(s1);// テクスチャ・サンプラ

// **************************************************
// シャドウマップを使う3Dオブジェクトの描画
// **************************************************

// ライティング計算
float lighting(PS_OUT pin)
{
	// 光源ベクトル
	float3 light = light_direction.xyz - pin.PosSM.xyz;
	// 距離
	float  leng = length(light);
	// 明るさ
	return 30 * dot(normalize(light), 0) / pow(leng, 2);
}

// ピクセル シェーダの関数
float4 main(PS_OUT pin) : SV_TARGET
{
	// ライティング計算
	float bright = lighting(pin);

// テクスチャ取得
float4 texCol = diffuse_map.Sample(diffuse_map_sampler_state, pin.texcoord);         // テクセル読み込み

																					 // シャドウマップ
float sm = ShadowMap.Sample(smpBorder, pin.PosSM.xy);
float sma = (pin.PosSM.z < sm) ? 1.0 : 0.5;

// 色
return saturate(bright * texCol /** Diffuse*/ * sma);
}