#pragma once

#define STRICT					// 型チェックを厳密に行なう
#define WIN32_LEAN_AND_MEAN		// ヘッダーからあまり使われない関数を省く

#include <windows.h>
#include <tchar.h>
#include <sstream>

#include "misc.h"
#include "high_resolution_timer.h"

#include <d3d11.h>//追加
#include <dxgi.h>
//#include "sprite2D.h"
#include "Blend.h"
#include "Font.h"
#include "GeometricPrimitive.h"
// unit12
#include "Static_mesh.h"
// UNIT.16
#include "Skinned_mesh.h"

#pragma comment (lib,"d3d11.lib")
#pragma comment (lib,"dxgi.lib")


#define THREAD_NUM_MAX 4//7個まで
class ResourceManager;
class framework
{
private:
	ID3D11Device* p_Device;
	IDXGIFactory* p_Factory;//スワップチェイン
	ID3D11DeviceContext* p_ImidiateContext;
	ID3D11DeviceContext* p_DeferredContext[THREAD_NUM_MAX];//遅延コンテキスト
	ID3D11CommandList* p_CommandList[THREAD_NUM_MAX];
	IDXGISwapChain* p_SwapChain;
	ID3D11RenderTargetView* p_RenderTargetView;
	ID3D11DepthStencilView* p_DepthStencilView;

	DirectX::XMFLOAT4 materialColor;
public:
	CONST HWND hwnd;
	static CONST LONG SCREEN_WIDTH = 1280;
	static CONST LONG SCREEN_HEIGHT = 720;

	////////////////////////////////////////
	//Added by Unit7
	///////////////////////////////////////
	ID3D11BlendState* p_BlendState;
	////////////////////////////////////////
	//Added by Unit10
	///////////////////////////////////////
	GeometricPrimitive *cube;
	Static_mesh* mesh[THREAD_NUM_MAX];
	Skinned_mesh* skin[2];

	////////////////////////////////////////
	//Added by Unit9
	///////////////////////////////////////
	LONGLONG CountsPerSecond;
	LONGLONG CountsPerFrame;//1/60秒
	LONGLONG NextCounts;


	////////////////////////////////////////
	//Castamed by Unit8
	///////////////////////////////////////
	framework(HWND hwnd) : hwnd(hwnd), p_Device(nullptr),p_ImidiateContext(nullptr),p_SwapChain(nullptr),p_RenderTargetView(nullptr),
	    p_DepthStencilView(nullptr)/*,particle(nullptr)*/
	{
	}
#define RELEASE_IF(x) if(x){x->Release();x=nullptr;}
#define DELETE_IF(x) if(x){delete x; x=NULL;}
	~framework()
	{
		// デバイスステートのクリア
		if (p_ImidiateContext)p_ImidiateContext->ClearState();

	    Font::ReleaseImage();
		BlendMode::Release();

		DELETE_IF(cube);
		for (int i = 0; i <THREAD_NUM_MAX; i++)
		{
			if (p_DeferredContext[i])p_DeferredContext[i]->ClearState();
			RELEASE_IF(p_DeferredContext[i]);
			DELETE_IF(mesh[i]);
			RELEASE_IF(p_CommandList[i])
		}

		DELETE_IF(skin[0]);
		DELETE_IF(skin[1]);
	    ///DELETE_IF(particle);
	    ///delete[] benchmarkTime;
	   /* for (int i = 0; i < 1024; i++) {
		DELETE_IF(sprites[i]);
	    }*/
	    ////////////////////////////////////////
	    //Added by Unit7
	    ///////////////////////////////////////
		// 取得したインターフェースのクリア

	    RELEASE_IF(p_DepthStencilView);
	    RELEASE_IF(p_RenderTargetView);
	    RELEASE_IF(p_SwapChain);
	    RELEASE_IF(p_ImidiateContext);
		RELEASE_IF(p_Factory);
	    RELEASE_IF(p_Device);
	}
#undef RELEASE_IF
#undef DELETE_IF
	int run()
	{
		MSG msg = {};

		if (!initialize()) return 0;

		// 時間計測準備
		LARGE_INTEGER LargeInteger;
		QueryPerformanceFrequency(&LargeInteger);
		CountsPerSecond = LargeInteger.QuadPart;
		CountsPerFrame = CountsPerSecond / 60;
		// 時間計測開始
		QueryPerformanceCounter(&LargeInteger);
		NextCounts = LargeInteger.QuadPart;

		while (WM_QUIT != msg.message)
		{
			if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
			else
			{
				//// フレーム制御
				LARGE_INTEGER count;
				QueryPerformanceCounter(&count);
#ifndef _DEBUG
				if (NextCounts > count.QuadPart)
				{
					DWORD t = (DWORD)((NextCounts - count.QuadPart) * 1000 / CountsPerSecond);
					Sleep(t);
					continue;
				}
#endif
				timer.tick();
				calculate_frame_stats();
				update(timer.time_interval());
				//render(timer.time_interval());
				StanbyPresant();

				// 次の活動予定時間
				NextCounts += CountsPerFrame;
			}
		}
		return static_cast<int>(msg.wParam);
	}

	LRESULT CALLBACK handle_message(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
	{
		switch (msg)
		{
		case WM_PAINT:
		{
			PAINTSTRUCT ps;
			HDC hdc;
			hdc = BeginPaint(hwnd, &ps);
			EndPaint(hwnd, &ps);
			break;
		}
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		case WM_CREATE:
			break;
		case WM_KEYDOWN:
			if (wparam == VK_ESCAPE) PostMessage(hwnd, WM_CLOSE, 0, 0);
			break;
		case WM_ENTERSIZEMOVE:
			// WM_EXITSIZEMOVE is sent when the user grabs the resize bars.
			timer.stop();
			break;
		case WM_EXITSIZEMOVE:
			// WM_EXITSIZEMOVE is sent when the user releases the resize bars.
			// Here we reset everything based on the new window dimensions.
			timer.start();
			break;
		default:
			return DefWindowProc(hwnd, msg, wparam, lparam);
		}
		return 0;
	}

private:
	bool initialize();
	void update(float elapsed_time/*Elapsed seconds from last frame*/);
	void render(float elapsed_time/*Elapsed seconds from last frame*/);
	// 無駄な画面描画の抑制
	bool StanbyPresant();//! 追加
	// マルチスレッド
	void ThreadFunc(Static_mesh** mesh, ID3D11CommandList ** ppCommandList, ID3D11DeviceContext * pDeferredContext, const DirectX::XMFLOAT4X4 & wvp, const DirectX::XMFLOAT4X4 & world, const DirectX::XMFLOAT4 & lightVector, const DirectX::XMFLOAT4 * materialColor, bool FlagPaint);

private:
	high_resolution_timer timer;
	void calculate_frame_stats()
	{
		// Code computes the average frames per second, and also the
		// average time it takes to render one frame.  These stats
		// are appended to the window caption bar.
		static int frames = 0;
		static float time_tlapsed = 0.0f;

		frames++;

		// Compute averages over one second period.
		if ((timer.time_stamp() - time_tlapsed) >= 1.0f)
		{
			float fps = static_cast<float>(frames); // fps = frameCnt / 1
			float mspf = 1000.0f / fps;
			std::ostringstream outs;
			outs.precision(6);
			outs << "FPS : " << fps << " / " << "Frame Time : " << mspf << " (ms)";
			SetWindowTextA(hwnd, outs.str().c_str());

			// Reset for next average.
			frames = 0;
			time_tlapsed += 1.0f;
		}
	}
};
