// UNIT.16
#include "Skinned_mesh.h"
#include <functional>
#include "ResourceManager.h"
#include "texture.h"
#define SCREEN_W 1280
#define SCREEN_H 720
//ボーン影響度
struct BONE_INFLUENCE
{
	int index;// index of bone
	float weight;// weight of bone
};
typedef std::vector<BONE_INFLUENCE> Bone_Influences_Per_ContorolPoint;

//ボーン影響度をFBXデータから取得する。
void fetch_bone_influences(const fbxsdk::FbxMesh* fbx_mesh, std::vector<Bone_Influences_Per_ContorolPoint>& infuluences)
{
	const int number_of_contorol_points = fbx_mesh->GetControlPointsCount();
	infuluences.resize(number_of_contorol_points);

	const int number_of_deformers = fbx_mesh->GetDeformerCount(fbxsdk::FbxDeformer::eSkin);
	for (int index_of_deformer = 0; index_of_deformer < number_of_deformers; ++index_of_deformer)
	{
		fbxsdk::FbxSkin* Skin = static_cast<fbxsdk::FbxSkin*>(fbx_mesh->GetDeformer(index_of_deformer, fbxsdk::FbxDeformer::eSkin));

		const int number_of_clusters = Skin->GetClusterCount();
		for (int index_of_cluster = 0; index_of_cluster < number_of_clusters; ++index_of_cluster)
		{
			fbxsdk::FbxCluster* cluster = Skin->GetCluster(index_of_cluster);

			const int number_of_contorol_point_indices = cluster->GetControlPointIndicesCount();
			const int* array_of_contorol_point_indices = cluster->GetControlPointIndices();
			const double* array_of_control_point_weights = cluster->GetControlPointWeights();

			for (int i = 0; i < number_of_contorol_point_indices; ++i)
			{
				Bone_Influences_Per_ContorolPoint& Influences_per_control_point = infuluences.at(array_of_contorol_point_indices[i]);
				BONE_INFLUENCE Influence;
				Influence.index = index_of_cluster;
				Influence.weight = static_cast<float>(array_of_control_point_weights[i]);
				Influences_per_control_point.push_back(Influence);
			}
		}
	}
}

// UNIT.22
//FBXの行列をXNA算術用の行列に変換
DirectX::XMFLOAT4X4* FbxMatrixToXMFloat4X4(DirectX::XMFLOAT4X4* _pOut, const FbxMatrix& _mtx)
{
	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			_pOut->m[i][j] = static_cast<float>(_mtx.Get(i, j));
		}
	}

	return _pOut;
}


// UNIT.22
//ボーン行列をFBXデータから取得する。
void fetch_bone_matrices(const fbxsdk::FbxMesh* fbx_mesh, std::vector<Skinned_mesh::BONE>& skeltal, const fbxsdk::FbxTime time)
{
	const int number_of_deformers = fbx_mesh->GetDeformerCount(fbxsdk::FbxDeformer::eSkin);
	for (int index_of_deformers = 0; index_of_deformers < number_of_deformers; index_of_deformers++)
	{
		fbxsdk::FbxSkin* Skin = static_cast<fbxsdk::FbxSkin*>(fbx_mesh->GetDeformer(index_of_deformers, fbxsdk::FbxDeformer::eSkin));

		const int number_of_clusters = Skin->GetClusterCount();
		skeltal.resize(number_of_clusters);
		for (int index_of_cluster = 0; index_of_cluster < number_of_clusters; index_of_cluster++)
		{
			Skinned_mesh::BONE* Bone = &skeltal.at(index_of_cluster);
			fbxsdk::FbxCluster* Cluster = Skin->GetCluster(index_of_cluster);

			// this matrix trnasforms coordinates of the initial pose from mesh space to global space
			fbxsdk::FbxAMatrix Reference_global_init_position;
			Cluster->GetTransformMatrix(Reference_global_init_position);

			// this matrix trnasforms coordinates of the initial pose from bone space to global space
			fbxsdk::FbxAMatrix  Cluster_global_init_position;
			Cluster->GetTransformLinkMatrix(Cluster_global_init_position);

			// this matrix trnasforms coordinates of the current pose from bone space to global space
			fbxsdk::FbxAMatrix Cluster_global_current_position;
			Cluster_global_current_position = Cluster->GetLink()->EvaluateGlobalTransform(time);

			// this matrix trnasforms coordinates of the current pose from mesh space to global space
			fbxsdk::FbxAMatrix Reference_global_current_position;
			Reference_global_current_position = fbx_mesh->GetNode()->EvaluateGlobalTransform(time);

			// Matrices are defined using the Column Major scheme. When a FbxAMatrix represents a transformation
			// (translation, rotation and scale), the last row of the matrix represents the translation part of the
			// transformation.
			fbxsdk::FbxAMatrix Transform = Reference_global_current_position.Inverse()*Cluster_global_current_position
				*Cluster_global_init_position.Inverse()*Reference_global_init_position;

			// convert FbxAMatrix(transform) to XMDLOAT4X4(bone.transform)
			FbxMatrixToXMFloat4X4(&Bone->transform, Transform);

		}
	}
}






/*-------------------------------------------
シャドウ マップの初期化
--------------------------------------------*/
HRESULT Skinned_mesh::InitShadowMap(ID3D11Device * p_Device)
{
	HRESULT hr = S_OK;


	// シャドウ マップの作成
	D3D11_INPUT_ELEMENT_DESC InputElementDesk[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		// UNIT.17
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA ,0 },
		// UNIT.20
		{ "WEIGHTS",0,DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "BONES",0,DXGI_FORMAT_R32G32B32A32_UINT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	const UINT ELEMENTS_ARRAY_NUM = ARRAYSIZE(InputElementDesk);
	/////////////////////////////////////////////////
	// �Aバーテックスシェーダーオブジェクトの生成
	/////////////////////////////////////////////////
	bool f = true;
	//f=ResourceManager::LoadVertexShader(p_Device, "skinned_mesh_vs.cso", InputElementDesk, ELEMENTS_ARRAY_NUM, &p_VertexShader, &p_InputLayout);
	f = ResourceManager::LoadVertexShader(p_Device, "ShadowShader_vs.cso", InputElementDesk, ELEMENTS_ARRAY_NUM, &p_VertexShaderShadow, &p_InputLayoutShadow);
	if (!f)
	{
		assert(!"データが見つからなかった");
		return hr;
	}
	/////////////////////////////////////////////////
	// �Bピクセルシェーダーオブジェクトの生成
	/////////////////////////////////////////////////
	//f = ResourceManager::LoadPixelShader(p_Device, "skinned_mesh_ps.cso", &p_PixelShader);
	f = ResourceManager::LoadPixelShader(p_Device, "ShadowShader_ps.cso", &p_PixelShaderShadow);
	if (!f)
	{
		assert(!"データが見つからなかった");
		return hr;
	}

	////////////////////////////////////////////////////////
	// �Cラスタライザーステートオブジェクトの生成（線描画・塗りつぶし描画)
	////////////////////////////////////////////////////////
	D3D11_RASTERIZER_DESC RasteriserDesk;
	ZeroMemory(&RasteriserDesk, sizeof(D3D11_RASTERIZER_DESC));
	D3D11_RASTERIZER_DESC RSDesc;
	RSDesc.FillMode = D3D11_FILL_SOLID;   // 普通に描画する
	RSDesc.CullMode = D3D11_CULL_FRONT;   // 表面を描画する
	RSDesc.FrontCounterClockwise = FALSE; // 時計回りが表面
	RSDesc.DepthBias = 0;
	RSDesc.DepthBiasClamp = 0;
	RSDesc.SlopeScaledDepthBias = 0;
	RSDesc.DepthClipEnable = TRUE;
	RSDesc.ScissorEnable = FALSE;
	RSDesc.MultisampleEnable = FALSE;
	RSDesc.AntialiasedLineEnable = FALSE;
	hr = p_Device->CreateRasterizerState(&RSDesc, &p_RasterizerStateShadow);
	if (FAILED(hr))
	{
		assert(!"ラスタライザーステートオブジェクトの生成ができません");
		return hr;
	}












	D3D11_TEXTURE2D_DESC shadowMapDesc;
	shadowMapDesc.Width = SCREEN_W;   // 幅
	shadowMapDesc.Height = SCREEN_H;  // 高さ
	shadowMapDesc.MipLevels = 1;       // ミップマップ レベル数
	shadowMapDesc.ArraySize = 1;       // 配列サイズ
	shadowMapDesc.Format = DXGI_FORMAT_R32_TYPELESS;  // フォーマット
	shadowMapDesc.SampleDesc.Count = 1;  // マルチサンプリングの設定
	shadowMapDesc.SampleDesc.Quality = 0;  // マルチサンプリングの品質
	shadowMapDesc.Usage = D3D11_USAGE_DEFAULT;      // デフォルト使用法
	shadowMapDesc.BindFlags = D3D11_BIND_RENDER_TARGET /*| D3D11_BIND_SHADER_RESOURCE */| D3D11_BIND_DEPTH_STENCIL; // 深度/ステンシル、シェーダ リソース ビューとして使用
	shadowMapDesc.CPUAccessFlags = 0;   // CPUからはアクセスしない
	shadowMapDesc.MiscFlags = 0;   // その他の設定なし
	hr = p_Device->CreateTexture2D(
		&shadowMapDesc,         // 作成する2Dテクスチャの設定
		NULL,               //
		&p_ShadowMap);     // 作成したテクスチャを受け取る変数
	if (FAILED(hr))
	{
		assert(!"シャドウ マップの作成ができません");
		return hr;
	}
	p_Device->CreateRenderTargetView(p_ShadowMap, nullptr, &p_RenderTargetView);
	if (FAILED(hr))
	{
		assert(!"シャドウ マップの作成ができません");
		return hr;
	}
	// 深度/ステンシル ビューの作成
	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	ZeroMemory(&descDSV, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));
	descDSV.ViewDimension = /*D3D11_DSV_DIMENSION_TEXTURE2DMS*/D3D11_DSV_DIMENSION_TEXTURE2D;
	descDSV.Flags = 0;
	descDSV.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	descDSV.Texture2D.MipSlice = 0;
	hr = p_Device->CreateDepthStencilView(
		p_ShadowMap,         // 深度/ステンシル・ビューを作るテクスチャ
		&descDSV,             // 深度/ステンシル・ビューの設定
		&p_ShadowMapDSView); // 作成したビューを受け取る変数
	if (FAILED(hr))
	{
		assert(!"シャドウ マップの作成生成ができません");
		return hr;
	}

	// シェーダ リソース ビューの作成
	D3D11_SHADER_RESOURCE_VIEW_DESC srDesc;
	ZeroMemory(&srDesc, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
	srDesc.Format = DXGI_FORMAT_R32_FLOAT; // フォーマット
	srDesc.ViewDimension = D3D_SRV_DIMENSION_TEXTURE2D;  // 2Dテクスチャ
	srDesc.Texture2D.MostDetailedMip = 0;   // 最初のミップマップ レベル
	srDesc.Texture2D.MipLevels = -1;  // すべてのミップマップ レベル

	// シェーダ リソース ビューの作成
	hr = p_Device->CreateShaderResourceView(
		p_ShadowMap,          // アクセスするテクスチャ リソース
		&srDesc,               // シェーダ リソース ビューの設定
		&p_ShaderResourceShadow);  // 受け取る変数
	if (FAILED(hr))
	{
		assert(!"シャドウ マップの作成生成ができません");
		return hr;
	}

	// ビューポートの設定
	ViewPortShadowMap.TopLeftX = 0.0f;		// ビューポート領域の左上X座標。
	ViewPortShadowMap.TopLeftY = 0.0f;		// ビューポート領域の左上Y座標。
	ViewPortShadowMap.Width = static_cast<FLOAT>(SCREEN_W);	// ビューポート領域の幅
	ViewPortShadowMap.Height = static_cast<FLOAT>(SCREEN_H);	// ビューポート領域の高さ
	ViewPortShadowMap.MinDepth = 0.0f;		// ビューポート領域の深度値の最小値
	ViewPortShadowMap.MaxDepth = 1.0f;		// ビューポート領域の深度値の最大値


	return S_OK;
}

















Skinned_mesh::Skinned_mesh(ID3D11Device * p_Device, const char * fbx_filename)
{
	//　FBXファイルのロード---------------------------
	loadFbxFile(p_Device, fbx_filename);

	// COMオブジェクトの初期化-----------------------
	HRESULT hr = S_OK;

	/////////////////////////////////////////////////
	// �@頂点データの構造を記述(記載した情報をIAステージに伝える)
	/////////////////////////////////////////////////
	D3D11_INPUT_ELEMENT_DESC InputElementDesk[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		// UNIT.17
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA ,0 },
		// UNIT.20
		{ "WEIGHTS",0,DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "BONES",0,DXGI_FORMAT_R32G32B32A32_UINT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	const UINT ELEMENTS_ARRAY_NUM = ARRAYSIZE(InputElementDesk);
	/////////////////////////////////////////////////
	// �Aバーテックスシェーダーオブジェクトの生成
	/////////////////////////////////////////////////
	bool f = true;
	f=ResourceManager::LoadVertexShader(p_Device, "skinned_mesh_vs.cso", InputElementDesk, ELEMENTS_ARRAY_NUM, &p_VertexShader, &p_InputLayout);
	//f = ResourceManager::LoadVertexShader(p_Device, "ShadowShader_vs.cso", InputElementDesk, ELEMENTS_ARRAY_NUM, &p_VertexShader, &p_InputLayout);
	if (!f)
	{
		assert(!"データが見つからなかった");
		return;
	}
	/////////////////////////////////////////////////
	// �Bピクセルシェーダーオブジェクトの生成
	/////////////////////////////////////////////////
	f = ResourceManager::LoadPixelShader(p_Device, "skinned_mesh_ps.cso", &p_PixelShader);
	//f = ResourceManager::LoadPixelShader(p_Device, "ShadowShader_ps.cso", &p_PixelShader);
	if (!f)
	{
		assert(!"データが見つからなかった");
		return;
	}

	////////////////////////////////////////////////////////
	// �Cラスタライザーステートオブジェクトの生成（線描画・塗りつぶし描画)
	////////////////////////////////////////////////////////
	D3D11_RASTERIZER_DESC RasteriserDesk;
	ZeroMemory(&RasteriserDesk, sizeof(D3D11_RASTERIZER_DESC));
	//-線描画の場合
	RasteriserDesk.FillMode = D3D11_FILL_WIREFRAME;	//レンダリング時に使用する描画モードを決定します
	RasteriserDesk.CullMode = D3D11_CULL_NONE;		//特定の方向を向いている三角形の描画の有無を示します。
	RasteriserDesk.FrontCounterClockwise = FALSE;	//三角形が前向きか後ろ向きかを決定します。
	RasteriserDesk.DepthBias = 0;					//指定のピクセルに加算する深度値です。
	RasteriserDesk.DepthBiasClamp = 0;				//ピクセルの最大深度バイアスです。
	RasteriserDesk.SlopeScaledDepthBias = 0;		//指定のピクセルのスロープに対するスカラです。
	RasteriserDesk.DepthClipEnable = FALSE;			//距離に基づいてクリッピングを有効にします。
	RasteriserDesk.ScissorEnable = FALSE;			//シザーカリング
	RasteriserDesk.MultisampleEnable = FALSE;		//マルチサンプリングのアンチエイリアシング
	RasteriserDesk.AntialiasedLineEnable = TRUE;	//線のアンチエイリアシング(線を描画中で MultisampleEnable が false の場合にのみ)
	hr = p_Device->CreateRasterizerState(&RasteriserDesk, &p_RasterizerStateLine);
	if (FAILED(hr))
	{
		assert(!"ラスタライザーステートオブジェクトの生成ができません");
		return;
	}

	//-塗りつぶし描画の場合
	RasteriserDesk.FillMode = D3D11_FILL_SOLID;		//レンダリング時に使用する描画モードを決定します
	RasteriserDesk.CullMode = /*D3D11_CULL_BACK*//*D3D11_CULL_FRONT*/D3D11_CULL_NONE;		//特定の方向を向いている三角形の描画の有無を示します。
	RasteriserDesk.MultisampleEnable = TRUE;		//マルチサンプリングのアンチエイリアシング
	RasteriserDesk.AntialiasedLineEnable = FALSE;	//線のアンチエイリアシング(線を描画中で MultisampleEnable が false の場合にのみ)
	hr = p_Device->CreateRasterizerState(&RasteriserDesk, &p_RasterizerStatePaint);
	if (FAILED(hr))
	{
		assert(!"ラスタライザーステートオブジェクト(塗りつぶし描画)の生成ができません");
		return;
	}

	/////////////////////////////////////////////////
	// �D深度ステンシル ステート オブジェクトの生成
	/////////////////////////////////////////////////
	D3D11_DEPTH_STENCIL_DESC DepthDesc;
	ZeroMemory(&DepthDesc, sizeof(DepthDesc));
	DepthDesc.DepthEnable = TRUE;									//深度テストあり
	DepthDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;			//書き込む
	DepthDesc.DepthFunc = D3D11_COMPARISON_LESS;					//手前の物体を描画
	DepthDesc.StencilEnable = FALSE;								//ステンシル テストなし
	DepthDesc.StencilReadMask = D3D11_DEFAULT_STENCIL_READ_MASK;		//ステンシル読み込みマスク
	DepthDesc.StencilWriteMask = D3D11_DEFAULT_STENCIL_WRITE_MASK;	//ステンシル書き込みマスク
	//面が表を向いている場合のステンシルステートの設定
	DepthDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;		  //維持
	DepthDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;	  //維持
	DepthDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;		  //維持
	DepthDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;		  //常に成功
	 //面が裏を向いている場合のステンシルステートの設定
	DepthDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;		  //維持
	DepthDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;	  //維持
	DepthDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;		  //維持
	DepthDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;		  //常に成功
	hr = p_Device->CreateDepthStencilState(&DepthDesc, &p_DepthStencilState);	//
	if (FAILED(hr))
	{
		assert(!"深度ステンシル ステート オブジェクトの生成ができません");
		return;
	}
	// �H定数バッファオブジェクトの生成
	D3D11_BUFFER_DESC ConstantBufferDesc;
	ZeroMemory(&ConstantBufferDesc, sizeof(ConstantBufferDesc));
	ConstantBufferDesc.Usage = /*D3D11_USAGE_DYNAMIC*/D3D11_USAGE_DEFAULT;			//動的使用法
	ConstantBufferDesc.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_CONSTANT_BUFFER;//定数バッファ
	ConstantBufferDesc.CPUAccessFlags = 0/*D3D11_CPU_ACCESS_WRITE*/;//CPUから書き込む
	ConstantBufferDesc.MiscFlags = 0;
	ConstantBufferDesc.ByteWidth = sizeof(CONSTANT_BUFFER);
	ConstantBufferDesc.StructureByteStride = 0;


	hr = p_Device->CreateBuffer(&ConstantBufferDesc, nullptr, &p_BufferConst);
	if (FAILED(hr))
	{
		assert(!"定数バッファオブジェクトの生成ができません");
		return;
	}
	/////////////////////////////////////////////////
	// サンプラーステートオブジェクトの設定（テクスチャの描画）
	/////////////////////////////////////////////////
	D3D11_SAMPLER_DESC SamplerDesc;
	SamplerDesc.Filter = D3D11_FILTER_ANISOTROPIC;		 //異方性フィルタリング
	SamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;	 //「ラップ・テクスチャ」アドレシング・モード
	SamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;	 //
	SamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;	 //
	SamplerDesc.MipLODBias = 0.0f;						//ミップマップの詳細レベル
	SamplerDesc.MaxAnisotropy = 16;					//異方性フィルタリングの次数
	SamplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS/*D3D11_COMPARISON_NEVER*/; //
	memcpy(SamplerDesc.BorderColor, &DirectX::XMFLOAT4(0, 0, 0, 0), sizeof(DirectX::XMFLOAT4));
	/*SamplerDesc.BorderColor[0] = 0.0f;
	SamplerDesc.BorderColor[1] = 0.0f;
	SamplerDesc.BorderColor[2] = 0.0f;
	SamplerDesc.BorderColor[3] = 0.0f;*/
	SamplerDesc.MinLOD = 0;						 //0が最大で最も精細//-FLT_MAX;
	SamplerDesc.MaxLOD = FLT_MAX;

	// サンプラー・ステート・オブジェクトの作成
	hr = p_Device->CreateSamplerState(&SamplerDesc, &p_SampleState[0]);
	if (FAILED(hr))
	{
		assert(!"サンプラー・ステート オブジェクトの生成ができません");
		return;
	}

	SamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_BORDER;
	SamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_BORDER;
	SamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_BORDER;
	SamplerDesc.BorderColor[0] = 1.0f;
	SamplerDesc.BorderColor[1] = 1.0f;
	SamplerDesc.BorderColor[2] = 1.0f;
	SamplerDesc.BorderColor[3] = 1.0f;
	hr = p_Device->CreateSamplerState(&SamplerDesc, &p_SampleState[1]);
	if (FAILED(hr))
	{
		assert(!"サンプラー・ステート2 オブジェクトの生成ができません");
		return;
	}

	//シャドウマップ初期化
	InitShadowMap(p_Device);
}




#define DELETE_IF(x) if(x){delete x;}
#define RELEASE_IF(x) if(x){x->Release();}
Skinned_mesh::~Skinned_mesh()
{
	RELEASE_IF(p_SampleState[0]);
	RELEASE_IF(p_SampleState[1]);
	RELEASE_IF(p_DepthStencilState);
	RELEASE_IF(p_RasterizerStateLine);
	RELEASE_IF(p_RasterizerStatePaint);
	RELEASE_IF(p_BufferConst);
	RELEASE_IF(p_ShadowMap);
	RELEASE_IF(p_ShadowMapDSView);
	ResourceManager::ReleasePixelShader(p_PixelShader);
	ResourceManager::ReleaseVertexShader(p_VertexShader, p_InputLayout);
	//追加
	ResourceManager::ReleasePixelShader(p_PixelShaderShadow);
	ResourceManager::ReleaseVertexShader(p_VertexShaderShadow, p_InputLayoutShadow);
	ResourceManager::ReleaseShaderResourceView(p_ShaderResourceShadow);
	RELEASE_IF(p_RenderTargetView);
	RELEASE_IF(p_RasterizerStateShadow);
	for (MESH& it : Meshes)
	{
		for (SUBSET& its : it.Subsets)
		{
			ResourceManager::ReleaseShaderResourceView(its.diffuse.p_Shader_resource_view);
		}
		it.p_IndexBuffer.ReleaseAndGetAddressOf();
		it.p_VertexBuffer.ReleaseAndGetAddressOf();

	}
	/*for (SUBSET& it : Subsets)
	{
		ResourceManager::ReleaseShaderResourceView(it.diffuse.shader_resource_view);
	}*/
}
#undef RELEASE_IF
#undef DELETE_IF



void Skinned_mesh::create_buffer(ID3D11Device * p_Device, MESH* mesh, VERTEX * vertices, const int NUM_VRETEX, UINT * indices, const int NUM_INDEX)
{
	HRESULT hr = S_OK;
	/////////////////////////////////////////////////
	// �F頂点バッファオブジェクトの生成
	//////////////////////////////////////////////////
	// �E頂点情報・インデックス情報のセット// 一辺が 1.0 の正立方体データを作成する（重心を原点にする

	// 頂点バッファ定義
	D3D11_BUFFER_DESC Bufferdesk;
	ZeroMemory(&Bufferdesk, sizeof(Bufferdesk));
	Bufferdesk.ByteWidth = NUM_VRETEX * sizeof(VERTEX);
	Bufferdesk.Usage = D3D11_USAGE_IMMUTABLE;	//GPUのみ
	Bufferdesk.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	Bufferdesk.CPUAccessFlags = 0;
	Bufferdesk.MiscFlags = 0;
	Bufferdesk.StructureByteStride = 0;//float?sizeof(DirectX::XMFLOAT3)

	// サブリソースの初期化に使用されるデータを指定します。
	D3D11_SUBRESOURCE_DATA SubResourceData;
	ZeroMemory(&SubResourceData, sizeof(SubResourceData));
	SubResourceData.pSysMem = vertices;				//(バッファの初期値)初期化データへのポインターです。
	SubResourceData.SysMemPitch = 0;					//テクスチャーにある 1 本の線の先端から隣の線までの距離 (バイト単位) です。
	SubResourceData.SysMemSlicePitch = 0;				//1 つの深度レベルの先端から隣の深度レベルまでの距離 (バイト単位) です。
	// バッファー (頂点バッファー、インデックス バッファー、またはシェーダー定数バッファー) を作成します。
	hr = p_Device->CreateBuffer(&Bufferdesk, &SubResourceData, &mesh->p_VertexBuffer);
	if (FAILED(hr))
	{
		assert(!"頂点バッファの作成ができません");
		return;
	}

	///////////////////////////////////////////////////
	// �Gインデックスバッファオブジェクトの生成
	///////////////////////////////////////////////////

	// インデックスバッファの定義
	D3D11_BUFFER_DESC IndexBufferDesc;
	ZeroMemory(&IndexBufferDesc, sizeof(IndexBufferDesc));
	IndexBufferDesc.ByteWidth = NUM_INDEX * sizeof(UINT);
	IndexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;	//GPUのみ
	IndexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	IndexBufferDesc.CPUAccessFlags = 0;
	IndexBufferDesc.MiscFlags = 0;
	IndexBufferDesc.StructureByteStride = 0;
	//インデックスの数を補完
	numIndices = NUM_INDEX;

	// インデックス・バッファのサブリソースの定義
	D3D11_SUBRESOURCE_DATA IndexSubResource;
	ZeroMemory(&IndexSubResource, sizeof(IndexSubResource));
	IndexSubResource.pSysMem = indices;
	IndexSubResource.SysMemPitch = 0;
	IndexSubResource.SysMemSlicePitch = 0;

	// インデックス・バッファの作成
	hr = p_Device->CreateBuffer(&IndexBufferDesc, &IndexSubResource, &mesh->p_IndexBuffer);
	if (FAILED(hr))
	{
		assert(!"インデックス・バッファの作成ができません");
		return;
	}

}






//定数
#define VERTEX_BUFFER_NUM 1//頂点バッファの数
//描画
void Skinned_mesh::render(ID3D11DeviceContext * p_DeviceContext, const DirectX::XMFLOAT4X4 & wvp, const DirectX::XMFLOAT4X4 & world, const DirectX::XMFLOAT4 & lightVector, const DirectX::XMFLOAT4 & materialColor, const DirectX::XMFLOAT4 &cameraPos, bool FlagPaint)
{
	if (1)
	{
		p_DeviceContext->OMSetRenderTargets(1, &p_RenderTargetView, p_ShadowMapDSView);
		//p_DeviceContext->ClearState();
		// 描画するプリミティブ種類の設定
		p_DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);//３角形リストを描画する//D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP
																					   // 入力レイアウト・オブジェクトの設定
		p_DeviceContext->IASetInputLayout(p_InputLayoutShadow);

		// シェーダーの設定;
		p_DeviceContext->VSSetShader(p_VertexShaderShadow, nullptr, 0);
		p_DeviceContext->PSSetShader(p_PixelShaderShadow, nullptr, 0);
		// ラスタライザ・ステート・オブジェクトの設定
		p_DeviceContext->RSSetState((p_RasterizerStateShadow));//ラスタライザステート
																								  ///p_DeviceContext->RSSetScissorRects();//シザー短形


																								  //震度ステンシルステート
		p_DeviceContext->OMSetDepthStencilState(p_DepthStencilState, 0);//震度ステンシルステート


		for (MESH& mesh : Meshes)
		{
			// �Bコンスタントバッファを設定+
			CONSTANT_BUFFER data = {};
			float x[4];
			data.light_direction = lightVector;
			data.cameraPos = cameraPos;
			for (int i = 0; i < 4; i++)
			{
				x[i] = DirectX::XMLoadFloat4(&cameraPos).m128_f32[i] * DirectX::XMLoadFloat4(&data.light_direction).m128_f32[i];

			}
			data.light_direction.x = x[0];
			data.light_direction.y = x[1];
			data.light_direction.z = x[2];
			data.light_direction.w = x[3];
			//XMStoreFloat4x4(&data.SMWorldViewProj, wvp);
			// UNIT.19
			//姿勢行列とwvp座標を掛け合わせる
			DirectX::XMStoreFloat4x4(&data.wvp,
				DirectX::XMLoadFloat4x4(&mesh.GloabalTransform)*DirectX::XMLoadFloat4x4(&wvp));
			//姿勢行列とワールド座標を掛け合わせる
			DirectX::XMStoreFloat4x4(&data.world,
				DirectX::XMLoadFloat4x4(&mesh.GloabalTransform)*DirectX::XMLoadFloat4x4(&world));

			// UNIT.21ダミー行列//deleted by UNIT22
			static float angle = 0;
			DirectX::XMStoreFloat4x4(&data.bone_transforms[0], DirectX::XMMatrixIdentity());
			DirectX::XMStoreFloat4x4(&data.bone_transforms[1], DirectX::XMMatrixRotationRollPitchYaw(0, 0, angle*0.01745f));
			DirectX::XMStoreFloat4x4(&data.bone_transforms[2], DirectX::XMMatrixIdentity());
			angle += 0.1f;

			//// UNIT.22
			//std::vector<BONE>& skeltal = mesh.Skeletal;
			//for (size_t i = 0; i < skeltal.size(); i++)
			//{
			//	DirectX::XMStoreFloat4x4(&data.bone_transforms[i], DirectX::XMLoadFloat4x4(&skeltal.at(i).transform));
			//}

			// UNIT.21
			// 反転
			/*DirectX::XMStoreFloat4x4(&data.wvp,
				DirectX::XMLoadFloat4x4(&mesh.GloabalTransform) * XMLoadFloat4x4(&coordinate_conversion) * DirectX::XMLoadFloat4x4(&wvp));
			DirectX::XMStoreFloat4x4(&data.world,
				DirectX::XMLoadFloat4x4(&mesh.GloabalTransform) *    XMLoadFloat4x4(&coordinate_conversion) *    XMLoadFloat4x4(&world));
*/

			// �@IAに頂点バッファを設定
			UINT stride[VERTEX_BUFFER_NUM] = { sizeof(VERTEX) };	 //頂点バッファにふくまれる頂点データのサイズ。
			UINT offset[VERTEX_BUFFER_NUM] = { 0 };					 //頂点バッファのオフセット
			p_DeviceContext->IASetVertexBuffers(0, VERTEX_BUFFER_NUM, nullptr, stride, offset);
			// �AIAにインデックスバッファを設定
			p_DeviceContext->IASetIndexBuffer(nullptr, DXGI_FORMAT_R32_UINT, 0);

			//for (SUBSET& its : mesh.Subsets)
			{
				// �Bコンスタントバッファを設定+
				//----------------
				data.material_color = materialColor;

				p_DeviceContext->UpdateSubresource(p_BufferConst, 0, nullptr, &data, 0, 0);//情報を定数バッファへコピー
				p_DeviceContext->VSSetConstantBuffers(0, VERTEX_BUFFER_NUM, &p_BufferConst);//定数バッファをシェーダへセット
				// PSに定数バッファを設定
				p_DeviceContext->PSSetConstantBuffers(0, 1, &p_BufferConst);
				// サンプラーステートのセット
				p_DeviceContext->PSSetSamplers(0, 2, p_SampleState);
				// シェーダリソースビューのセット
				//if (its.diffuse.p_Shader_resource_view)
					p_DeviceContext->PSSetShaderResources(0, 1, &p_ShaderResourceShadow);
					//p_DeviceContext->PSSetShaderResources(1, 2, &p_ShaderResourceShadow);
				// インデックス付けされているプリミティブの描画
					for (SUBSET& its : mesh.Subsets)
					{
						p_DeviceContext->DrawIndexed(its.index_count, its.index_start, 0);
					}
			}
		}
	}

	// ***************************************
	//float g_ClearColor[4] = { 0.0f, 0.125f, 0.3f, 1.0f };
	//// 描画ターゲットのクリア
	//p_DeviceContext->ClearRenderTargetView(
	//	p_RenderTargetView, // クリアする描画ターゲット
	//	g_ClearColor);         // クリアする値

	//						   // 深度/ステンシルのクリア
	//p_DeviceContext->ClearDepthStencilView(
	//	p_DepthStencilState, // クリアする深度/ステンシル・ビュー
	//	D3D11_CLEAR_DEPTH,   // 深度値だけをクリアする
	//	1.0f,                // 深度バッファをクリアする値
	//	0);                  // ステンシル・バッファをクリアする値(この場合、無関係)

	//						 // ***************************************
	//p_DeviceContext->ClearState();



	// 描画するプリミティブ種類の設定
	p_DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);//３角形リストを描画する//D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP
	// 入力レイアウト・オブジェクトの設定
	p_DeviceContext->IASetInputLayout(p_InputLayout);

	// シェーダーの設定;
	p_DeviceContext->VSSetShader(p_VertexShader, nullptr, 0);
	p_DeviceContext->PSSetShader(p_PixelShader, nullptr, 0);
	// ラスタライザ・ステート・オブジェクトの設定
	p_DeviceContext->RSSetState((FlagPaint ? p_RasterizerStatePaint : p_RasterizerStateLine));//ラスタライザステート
	///p_DeviceContext->RSSetScissorRects();//シザー短形


	//震度ステンシルステート
	p_DeviceContext->OMSetDepthStencilState(p_DepthStencilState, 0);//震度ステンシルステート


	for (MESH& mesh : Meshes)
	{
		// �Bコンスタントバッファを設定+
		CONSTANT_BUFFER data = {};

		data.light_direction = lightVector;
		data.lightColor = { 0.995f,0.995f,0.999f,0.99f };
		data.nyutoralLightColor = { 0.20f,0.21f,0.20f,1 };
		data.cameraPos = cameraPos;
		//XMStoreFloat4x4(&data.SMWorldViewProj, wvp);
		float x[4];
		for (int i = 0; i < 4; i++)
		{
			x[i] = DirectX::XMLoadFloat4(&cameraPos).m128_f32[i] * DirectX::XMLoadFloat4(&data.light_direction).m128_f32[i];

		}
		data.light_direction.x = x[0];
		data.light_direction.y = x[1];
		data.light_direction.z = x[2];
		data.light_direction.w = x[3];
		// UNIT.19
		//姿勢行列とwvp座標を掛け合わせる
		DirectX::XMStoreFloat4x4(&data.wvp,
			DirectX::XMLoadFloat4x4(&mesh.GloabalTransform)*DirectX::XMLoadFloat4x4(&wvp));
		//姿勢行列とワールド座標を掛け合わせる
		DirectX::XMStoreFloat4x4(&data.world,
			DirectX::XMLoadFloat4x4(&mesh.GloabalTransform)*DirectX::XMLoadFloat4x4(&world));

		// UNIT.21ダミー行列//deleted by UNIT22
		static float angle = 0;
		DirectX::XMStoreFloat4x4(&data.bone_transforms[0], DirectX::XMMatrixIdentity());
		DirectX::XMStoreFloat4x4(&data.bone_transforms[1], DirectX::XMMatrixRotationRollPitchYaw(0, 0, angle*0.01745f));
		DirectX::XMStoreFloat4x4(&data.bone_transforms[2], DirectX::XMMatrixIdentity());
		angle += 0.1f;

		//// UNIT.22
		//std::vector<BONE>& skeltal = mesh.Skeletal;
		//for (size_t i = 0; i < skeltal.size(); i++)
		//{
		//	DirectX::XMStoreFloat4x4(&data.bone_transforms[i], DirectX::XMLoadFloat4x4(&skeltal.at(i).transform));
		//}

		// UNIT.21
		// 反転
		DirectX::XMStoreFloat4x4(&data.wvp,
			DirectX::XMLoadFloat4x4(&mesh.GloabalTransform) * XMLoadFloat4x4(&coordinate_conversion) * DirectX::XMLoadFloat4x4(&wvp));
		DirectX::XMStoreFloat4x4(&data.world,
			DirectX::XMLoadFloat4x4(&mesh.GloabalTransform) *    XMLoadFloat4x4(&coordinate_conversion) *    XMLoadFloat4x4(&world));


		// �@IAに頂点バッファを設定
		UINT stride[VERTEX_BUFFER_NUM] = { sizeof(VERTEX) };	 //頂点バッファにふくまれる頂点データのサイズ。
		UINT offset[VERTEX_BUFFER_NUM] = { 0 };					 //頂点バッファのオフセット
		p_DeviceContext->IASetVertexBuffers(0, VERTEX_BUFFER_NUM, mesh.p_VertexBuffer.GetAddressOf(), stride, offset);
		// �AIAにインデックスバッファを設定
		p_DeviceContext->IASetIndexBuffer(mesh.p_IndexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);

		for (SUBSET& its : mesh.Subsets)
		{
			// �Bコンスタントバッファを設定+
			//----------------
			//data.material_color = materialColor;
			data.material_color.x = materialColor.x*its.diffuse.color.x;
			data.material_color.y = materialColor.y*its.diffuse.color.y;
			data.material_color.z = materialColor.z*its.diffuse.color.z;
			data.material_color.w = materialColor.w*its.diffuse.color.w;

			p_DeviceContext->UpdateSubresource(p_BufferConst, 0, nullptr, &data, 0, 0);//情報を定数バッファへコピー
			p_DeviceContext->VSSetConstantBuffers(0, VERTEX_BUFFER_NUM, &p_BufferConst);//定数バッファをシェーダへセット

			// サンプラーステートのセット
			p_DeviceContext->PSSetSamplers(0, 2, p_SampleState);
			// シェーダリソースビューのセット
			if (its.diffuse.p_Shader_resource_view)
				p_DeviceContext->PSSetShaderResources(0, 1, &its.diffuse.p_Shader_resource_view);
			// インデックス付けされているプリミティブの描画
			p_DeviceContext->DrawIndexed(its.index_count, its.index_start, 0);
		}
	}

}
//
//void Skinned_mesh::renderShadowMap(ID3D11DeviceContext * p_DeviceContext, const DirectX::XMFLOAT4X4 & wvp, const DirectX::XMFLOAT4X4 & world, const DirectX::XMFLOAT4 & lightVector, const DirectX::XMFLOAT4 & materialColor, const DirectX::XMFLOAT4 & cameraPos, bool FlagPaint)
//{
//	// 描画するプリミティブ種類の設定
//	p_DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);//３角形リストを描画する//D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP
//																				   // 入力レイアウト・オブジェクトの設定
//	p_DeviceContext->IASetInputLayout(p_InputLayout);
//
//	// シェーダーの設定;
//	p_DeviceContext->VSSetShader(p_VertexShader, nullptr, 0);
//	p_DeviceContext->PSSetShader(p_PixelShader, nullptr, 0);
//	// ラスタライザ・ステート・オブジェクトの設定
//	p_DeviceContext->RSSetState((FlagPaint ? p_RasterizerStatePaint : p_RasterizerStateLine));//ラスタライザステート
//																							  ///p_DeviceContext->RSSetScissorRects();//シザー短形
//
//
//																							  //震度ステンシルステート
//	p_DeviceContext->OMSetDepthStencilState(p_DepthStencilState, 0);//震度ステンシルステート
//	for (MESH& mesh : Meshes)
//	{
//		// �Bコンスタントバッファを設定+
//		CONSTANT_BUFFER data = {};
//
//		data.light_direction = lightVector;
//		data.cameraPos = cameraPos;
//
//		// UNIT.19
//		//姿勢行列とwvp座標を掛け合わせる
//		DirectX::XMStoreFloat4x4(&data.wvp,
//			DirectX::XMLoadFloat4x4(&mesh.GloabalTransform)*DirectX::XMLoadFloat4x4(&wvp));
//		//姿勢行列とワールド座標を掛け合わせる
//		DirectX::XMStoreFloat4x4(&data.world,
//			DirectX::XMLoadFloat4x4(&mesh.GloabalTransform)*DirectX::XMLoadFloat4x4(&world));
//
//		// UNIT.21ダミー行列//deleted by UNIT22
//		static float angle = 0;
//		DirectX::XMStoreFloat4x4(&data.bone_transforms[0], DirectX::XMMatrixIdentity());
//		DirectX::XMStoreFloat4x4(&data.bone_transforms[1], DirectX::XMMatrixRotationRollPitchYaw(0, 0, angle*0.01745f));
//		DirectX::XMStoreFloat4x4(&data.bone_transforms[2], DirectX::XMMatrixIdentity());
//		angle += 0.1f;
//
//		//// UNIT.22
//		//std::vector<BONE>& skeltal = mesh.Skeletal;
//		//for (size_t i = 0; i < skeltal.size(); i++)
//		//{
//		//	DirectX::XMStoreFloat4x4(&data.bone_transforms[i], DirectX::XMLoadFloat4x4(&skeltal.at(i).transform));
//		//}
//
//		// UNIT.21
//		// 反転
//		DirectX::XMStoreFloat4x4(&data.wvp,
//			DirectX::XMLoadFloat4x4(&mesh.GloabalTransform) * XMLoadFloat4x4(&coordinate_conversion) * DirectX::XMLoadFloat4x4(&wvp));
//		DirectX::XMStoreFloat4x4(&data.world,
//			DirectX::XMLoadFloat4x4(&mesh.GloabalTransform) *    XMLoadFloat4x4(&coordinate_conversion) *    XMLoadFloat4x4(&world));
//
//
//		// �@IAに頂点バッファを設定
//		UINT stride[VERTEX_BUFFER_NUM] = { sizeof(VERTEX) };	 //頂点バッファにふくまれる頂点データのサイズ。
//		UINT offset[VERTEX_BUFFER_NUM] = { 0 };					 //頂点バッファのオフセット
//		p_DeviceContext->IASetVertexBuffers(0, VERTEX_BUFFER_NUM, mesh.p_VertexBuffer.GetAddressOf(), stride, offset);
//		// �AIAにインデックスバッファを設定
//		p_DeviceContext->IASetIndexBuffer(mesh.p_IndexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);
//
//		for (SUBSET& its : mesh.Subsets)
//		{
//			// �Bコンスタントバッファを設定+
//			//----------------
//			//data.material_color = materialColor;
//			data.material_color.x = materialColor.x*its.diffuse.color.x;
//			data.material_color.y = materialColor.y*its.diffuse.color.y;
//			data.material_color.z = materialColor.z*its.diffuse.color.z;
//			data.material_color.w = materialColor.w*its.diffuse.color.w;
//
//			p_DeviceContext->UpdateSubresource(p_BufferConst, 0, nullptr, &data, 0, 0);//情報を定数バッファへコピー
//			p_DeviceContext->VSSetConstantBuffers(0, VERTEX_BUFFER_NUM, &p_BufferConst);//定数バッファをシェーダへセット
//
//																						// サンプラーステートのセット
//			p_DeviceContext->PSSetSamplers(0, 1, &p_SampleState);
//			// シェーダリソースビューのセット
//			if (its.diffuse.p_Shader_resource_view)
//				p_DeviceContext->PSSetShaderResources(0, 2, &its.diffuse.p_Shader_resource_view);
//			// インデックス付けされているプリミティブの描画
//			p_DeviceContext->DrawIndexed(its.index_count, its.index_start, 0);
//		}
//	}
//}











































// FBXデータロード
void Skinned_mesh::loadFbxFile(ID3D11Device * p_Device, const char * fbx_filename)
{
	//using namespace fbxsdk;
	// Create the FBX SDK manager
	fbxsdk::FbxManager* Manager = fbxsdk::FbxManager::Create();
	// メッシュデータ取得
	const std::vector<fbxsdk::FbxNode*> Fetched_meshes = GetFbxMesh(p_Device, fbx_filename, Manager);
	//メッシュ構造体初期化
	Meshes.resize(Fetched_meshes.size());

	// meshデータ代入
	for (size_t i = 0; i < Fetched_meshes.size(); i++)
	{
		const fbxsdk::FbxMesh* Fbx_mesh = Fetched_meshes.at(i)->GetMesh();// Fbx_mesh
		MESH* pMesh = &Meshes.at(i);// meshポインタ

		// UNIT.22
		/////////////////////////////////
		// ボーン行列取得
		/////////////////////////////////
		fbxsdk::FbxTime::EMode TimeMode = Fbx_mesh->GetScene()->GetGlobalSettings().GetTimeMode();
		fbxsdk::FbxTime FrameTime;
		FrameTime.SetTime(0, 0, 0, 1, 0, TimeMode);
		fetch_bone_matrices(Fbx_mesh, pMesh->Skeletal, FrameTime * 20);//20frame目のポーズ

		//////////////////////////////////
		// Fetch material properties.
		//////////////////////////////////
		FetchMaterial(p_Device, Fbx_mesh, pMesh, fbx_filename);

		// UNIT.18
		//////////////////////////////////
		// Count the polygon count of each material
		//////////////////////////////////
		SetIndexCount(Fbx_mesh, pMesh);



		//////////////////////////////////
		// Fetch mesh data
		//////////////////////////////////
		std::vector<VERTEX> vertices;// Vertex buffer
		std::vector<u_int> indices;// Index buffer
		FetchMeshData(p_Device, Fbx_mesh, pMesh, &vertices, &indices);

		// バッファの作成
		create_buffer(p_Device, pMesh, vertices.data(), vertices.size(), indices.data(), indices.size());
		FbxAMatrix global_transform = Fbx_mesh->GetNode()->EvaluateGlobalTransform(0);

		// UNIT.19
		//////////////////////////////////
		// 姿勢制御行列
		//////////////////////////////////
		fbxsdk::FbxMatrix GlobalTransform = Fbx_mesh->GetNode()->EvaluateGlobalTransform(0);
		for (u_int row = 0; row < 4; row++)//行
		{
			for (int column = 0; column < 4; column++)//列
			{
				pMesh->GloabalTransform.m[row][column] = static_cast<float>(GlobalTransform[row][column]);
			}
		}


	}

	Manager->Destroy();
}








// Fetch material properties.
void Skinned_mesh::FetchMaterial(ID3D11Device * p_Device, const fbxsdk::FbxMesh * Fbx_mesh, MESH* mesh, const char* fbx_filename)
{
	// UNIT.17
	// 材質情報を取得し、テクスチャを生成する。
	const int number_of_materials = Fbx_mesh->GetNode()->GetMaterialCount();
	if (number_of_materials > 0)
	{
		mesh->Subsets.resize(number_of_materials);// UNIT.18
	}
	else
	{
		mesh->Subsets.resize(1);// UNIT.18
	}
	//材質がない
	if (number_of_materials == 0)
	{
		SUBSET& pSubset = mesh->Subsets.at(0);// UNIT.18
		//ダミーテクスチャを貼る
		HRESULT hr = S_OK;
		hr = make_dummy_texture(p_Device, &pSubset.diffuse.p_Shader_resource_view);
		if (FAILED(hr))
		{
			assert(!"データが見つからなかった");
		}
	}
	for (int index_of_material = 0; index_of_material < number_of_materials; ++index_of_material)
	{
		SUBSET& pSubset = mesh->Subsets.at(index_of_material);// UNIT.18

		const fbxsdk::FbxSurfaceMaterial *surface_material = Fbx_mesh->GetNode()->GetMaterial(index_of_material);

		const fbxsdk::FbxProperty property = surface_material->FindProperty(fbxsdk::FbxSurfaceMaterial::sDiffuse);
		const fbxsdk::FbxProperty factor = surface_material->FindProperty(fbxsdk::FbxSurfaceMaterial::sDiffuseFactor);
		if (property.IsValid() )
		{
			if (factor.IsValid())
			{
				fbxsdk::FbxDouble3 color = property.Get<fbxsdk::FbxDouble3>();
				double f = factor.Get<fbxsdk::FbxDouble>();
				pSubset.diffuse.color.x = static_cast<float>(color[0] * f);
				pSubset.diffuse.color.y = static_cast<float>(color[1] * f);
				pSubset.diffuse.color.z = static_cast<float>(color[2] * f);
				pSubset.diffuse.color.w = 1.0f;
			}
			const int number_of_textures = property.GetSrcObjectCount<fbxsdk::FbxFileTexture>();
			const fbxsdk::FbxFileTexture* file_texture = property.GetSrcObject<fbxsdk::FbxFileTexture>();
			// テクスチャがあるか?
			if (number_of_textures == 0 || !file_texture)
			{
				//ダミーテクスチャを貼る
				HRESULT hr = S_OK;
				hr = make_dummy_texture(p_Device, &pSubset.diffuse.p_Shader_resource_view);
				if (FAILED(hr))
				{
					assert(!"データが見つからなかった");
					return;
				}
			}
			else
			{
				//ファイル名のテクスチャを貼る
				const char* filename = file_texture->GetRelativeFileName();//テクスチャファイル名
				wchar_t* wfilename = new wchar_t[strlen(filename) + 1];//テクスチャファイル名(wchar_t型に変換)
				//wchar_t* wfbx_filename = new wchar_t[strlen(fbx_filename) + 1];//fbxファイル名(wchar_t型に変換)
				size_t x;
				mbstowcs_s(&x, wfilename, strlen(filename) + 1, filename, _TRUNCATE);

				//mbstowcs_s(&x, wfbx_filename, strlen(filename) + 1, fbx_filename, _TRUNCATE);
				wchar_t combind_resource_path[256] = {};//ファイルパス
				//入ってるフォルダパスを１階層結合
				//CombineResourcePath(combind_resource_path, wfbx_filename, wfilename);

				// Create "diffuse.shader_resource_view" from "filename".
				D3D11_TEXTURE2D_DESC Texture2dDesc;
				bool f = true;
				f = ResourceManager::LoadShaderResourceView(p_Device, wfilename, &pSubset.diffuse.p_Shader_resource_view, &Texture2dDesc);
				if (!f)
				{
					if (wfilename)delete[] wfilename;
					//if (wfbx_filename)delete[] wfbx_filename;
					assert(!"データが見つからなかった");
					return;
				}
				if(wfilename)delete[] wfilename;
				wfilename = nullptr;
				//if (wfbx_filename)delete[] wfbx_filename;
				//wfbx_filename = nullptr;
			}
		}
	}

}











// Count the polygon count of each material
void Skinned_mesh::SetIndexCount(const fbxsdk::FbxMesh* Fbx_mesh, MESH* mesh)
{
	// Count the polygon count of each material
	const int number_of_polygons = Fbx_mesh->GetPolygonCount();
	const int number_of_materials = Fbx_mesh->GetNode()->GetMaterialCount();

	for (int index_of_poligon = 0; index_of_poligon < number_of_polygons; index_of_poligon++)
	{
		u_int index_of_material = 0;
		if (number_of_materials > 0)// これがないとcube000でバグになる。
		{
			index_of_material = Fbx_mesh->GetElementMaterial()->GetIndexArray().GetAt(index_of_poligon);// Question 材質番号
		}
		mesh->Subsets.at(index_of_material).index_count += 3;
	}

	// オフセット情報の記録（頂点が何個か？）
	int offset = 0;
	for (SUBSET& it : mesh->Subsets)
	{
		it.index_start = offset;
		offset += it.index_count;
		// This will be used as counter in the following procedures, reset to zero
		it.index_count = 0;// Question このまま呼んでいいのか？

	}
}













// Fetch mesh data
void Skinned_mesh::FetchMeshData(ID3D11Device* p_Device, const fbxsdk::FbxMesh* Fbx_mesh, MESH* mesh, std::vector<VERTEX>* vertices, std::vector<u_int>* indices)
{
	u_int vertex_count = 0;

	// UNIT.20
	//////////////////////////////////
	// ボーン影響度をFBXデータから取得する。
	//////////////////////////////////
	std::vector<Bone_Influences_Per_ContorolPoint> BoneInfluence;
	fetch_bone_influences(Fbx_mesh, BoneInfluence);

	// UNIT.17 uv座標の取得
	fbxsdk::FbxStringList uv_names;
	Fbx_mesh->GetUVSetNames(uv_names);
	//------------------------------

	// UNIT.16頂点データ取得ループ
	const fbxsdk::FbxVector4* array_of_contorol_points = Fbx_mesh->GetControlPoints();
	const int number_of_polygons = Fbx_mesh->GetPolygonCount();
	const int number_of_materials = Fbx_mesh->GetNode()->GetMaterialCount();// UNIT.18
	indices->resize(number_of_polygons * 3);// UNIT.18

	for (int index_of_poligon = 0; index_of_poligon < number_of_polygons; index_of_poligon++)
	{
		// UNIT.18----------------------------------------------------------------------
		// The material for current face.
		int index_of_material = 0;
		if (number_of_materials>0)
		{
			index_of_material = Fbx_mesh->GetElementMaterial()->GetIndexArray().GetAt(index_of_poligon);
		}
		// Where should I save the vertex attribute index, according to the material
		SUBSET& pSubset = mesh->Subsets.at(index_of_material);
		if (number_of_materials == 0)
		{
			mesh->Subsets.resize(1);
		}

		const int index_offset = pSubset.index_start + pSubset.index_count;
		//------------------------------------------------------------------------

		for (int index_of_vertex = 0; index_of_vertex < 3; index_of_vertex++)
		{
			// 頂点座標セット
			VERTEX vertexCopy;
			const int index_of_contorol_point = Fbx_mesh->GetPolygonVertex(index_of_poligon, index_of_vertex);
			vertexCopy.position.x = static_cast<float>(array_of_contorol_points[index_of_contorol_point][0]);
			vertexCopy.position.y = static_cast<float>(array_of_contorol_points[index_of_contorol_point][1]);
			vertexCopy.position.z = static_cast<float>(array_of_contorol_points[index_of_contorol_point][2]);

			// 法線ベクトルセット
			fbxsdk::FbxVector4 normal;
			Fbx_mesh->GetPolygonVertexNormal(index_of_poligon, index_of_vertex, normal);
			vertexCopy.normal.x = static_cast<float>(normal[0]);
			vertexCopy.normal.y = static_cast<float>(normal[1]);
			vertexCopy.normal.z = static_cast<float>(normal[2]);

			// UNIT.17------------------------------
			// uv座標セット
			if (number_of_materials>0)
			{
				fbxsdk::FbxVector2 uv;
				bool unmapped_uv;
				Fbx_mesh->GetPolygonVertexUV(index_of_poligon, index_of_vertex, uv_names[0], uv, unmapped_uv);
				vertexCopy.texcoord.x = static_cast<float>(uv[0]);
				vertexCopy.texcoord.y = 1.0f - static_cast<float>(uv[1]);
				//-----------------------------------------------------------
			}
			// UNIT.20
			// ボーン影響度セット
			auto& bone = BoneInfluence.at(index_of_contorol_point);
			for (u_int i = 0; i < bone.size(); i++)
			{
				if (i == MAX_BONE_INFLUENCES)break;//最大影響数でブレイク
				vertexCopy.bone_indices[i] = bone.at(i).index;
				vertexCopy.bone_weights[i] = bone.at(i).weight;
			}


			vertices->push_back(vertexCopy);
			//indices->push_back(vertex_count);
			indices->at(index_offset + index_of_vertex) = static_cast<u_int>(vertex_count);
			vertex_count += 1;
		}
		pSubset.index_count += 3;// UNIt.18
	}
}












// FBXメッシュ情報取得
std::vector<fbxsdk::FbxNode*> Skinned_mesh::GetFbxMesh(ID3D11Device* p_Device, const char* fbx_filename, fbxsdk::FbxManager* Manager)
{
	// Create an IOStettings object. IOSROOT is defined in Fbxiosettingspath.h.
	Manager->SetIOSettings(fbxsdk::FbxIOSettings::Create(Manager, IOSROOT));

	// Create an importer.
	fbxsdk::FbxImporter* importer = fbxsdk::FbxImporter::Create(Manager, "");

	// intialize the importer.
	bool import_status = false;
	import_status = importer->Initialize(fbx_filename, -1, Manager->GetIOSettings());
	_ASSERT_EXPR(import_status, importer->GetStatus().GetErrorString());

	// Create a new scene so it can be populated by the imported file.
	fbxsdk::FbxScene* Scene = fbxsdk::FbxScene::Create(Manager, "");

	// Import the contents of the file into the scene.
	import_status = importer->Import(Scene);
	_ASSERT_EXPR(import_status, importer->GetStatus().GetErrorString());

	// Convert mesh, NURBS and patch into triangle mesh
	fbxsdk::FbxGeometryConverter Geometry_converter(Manager);
	Geometry_converter.Triangulate(Scene,/*replace*/true);

	// Fetch node attributes and materials under this node recursively.Currentry only mesh.
	std::vector<fbxsdk::FbxNode*> Fetched_meshes;

	std::function<void(fbxsdk::FbxNode*)>Traverse = [&](fbxsdk::FbxNode* Node) {
		if (Node)
		{
			fbxsdk::FbxNodeAttribute* Fbx_node_attribute = Node->GetNodeAttribute();
			if (Fbx_node_attribute)
			{
				switch (Fbx_node_attribute->GetAttributeType())
				{
				case fbxsdk::FbxNodeAttribute::eMesh:
					Fetched_meshes.push_back(Node);
					break;
				default:
					break;
				}
			}
			for (int i = 0; i < Node->GetChildCount(); i++)
			{
				Traverse(Node->GetChild(i));
			}
		}
	};
	Traverse(Scene->GetRootNode());


	return  Fetched_meshes;
}

