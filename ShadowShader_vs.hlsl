#include "ShadowShader.hlsli"

#define NYUTORAL_LIGHT 0.2f//����

PS_OUT main(float4 position : POSITION, float4 normal : NORMAL, float2 texcoord : TEXCOORD,
	float4 bone_weights : WEIGHTS, uint4 bone_indices : BONES)// UNIT.20
{
	PS_OUT vout;


	// UNIT.21-----------
	float3 p = { 0, 0, 0 };
	float3 n = { 0, 0, 0 };
	for (int i2 = 0; i2 < 4; i2++)
	{
		if (bone_weights[i2] == 0)		continue;
		p += (bone_weights[i2] * mul(position, bone_transforms[bone_indices[i2]])).xyz;
		n += (bone_weights[i2] * mul(float4(normal.xyz, 0), bone_transforms[bone_indices[i2]])).xyz;
	}
	position = float4(p, 1.0f);
	normal = float4(n, 0.0f);
	//normal.w = 0;
	//-----------------------
	vout.position = mul(position, world_view_projection);
	vout.texcoord = texcoord;

	float4 N = normalize(mul(normal, world));

	float4 L = normalize(-light_direction);

	//���ʔ���
	/*float3 l = normalize(L.xyz - position.xyz);
	float3 no = normalize(normal.xyz);
	float3 r = 2.0 * no * dot(no, l) - l;
	float3 v = normalize(eyePos.xyz - position.xyz);
	float  i = pow(saturate(dot(r, v)), material_color.w);*/

	//�n�[�t�x�N�g�����ʔ���
	float3 l = normalize(L.xyz - position.xyz);
	float3 no = normalize(normal.xyz);
	float3 v = normalize(eyePos.xyz - position.xyz);
	float3 h = normalize(l + v);
	float  i = pow(saturate(dot(h, v)), material_color.w);

	vout.color = float4(i * material_color.xyz * L.xyz, 1.0);
	vout.color += material_color * max(0, dot(L, N)) + NYUTORAL_LIGHT;
	vout.color.a = material_color.a;

	// ���_���W�@���f�����W�n���������W�n(�V���h�E�}�b�v)
	float4 pos4 = mul(position, world_view_projection);
	pos4.xyz = pos4.xyz / pos4.w;
	vout.PosSM.x = (pos4.x + 1.0) / 2.0;
	vout.PosSM.y = (-pos4.y + 1.0) / 2.0;
	vout.PosSM.z = pos4.z;

	return vout;
}